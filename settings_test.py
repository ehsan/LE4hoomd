import numpy as np
import pandas as pd
import hoomd


##############################
# Simulation parameters
##############################
thermal_timesteps = 1e4
final_timesteps = 1e7
dump_period = 1e3
dump_fname = lambda sim_id : r'{}-dump.gsd'.format(sim_id)

# MD parameters
dt = 0.01
KT = 1

# Simulation box
N = 0  # do not change this number
N_pol = 500
L0 = 2*N_pol**0.5887
L = np.array([L0, L0, L0])
Lhalf = 0.5*L
box = hoomd.data.boxdim(*L)

# LJ potential
sigma = 1.0
epsilon = 3.1
fene_r0 = 1.6
lj_rcut_rep = sigma*2**(1./6.)  # act on all particles from different types

# loop-extrusion
LE_bond_type = 'harmonic'  # set to either 'harmonic' or 'fene'.
N_le = 10  # Number of loop extrusion agents/factors
le_period = 5e3  # Number of MD steps to update/slide the loop extrusion factor
le_p_off = 0.0125  # prob. of detachment at each le_period
le_extr_dist_thr = -1  # do not change it :)
le_type = 'loop_extrusion'
le_K = 10
le_r0 = 1.1
stop_probs = np.genfromtxt('CTCF.bed')[:, 1:]

##############################
# particle, bond and angle types
# read the particle information from a file
##############################
binder_bead_ratio = 0
typeid_fname = 'polymer.bed'
polymer_df = pd.read_csv(typeid_fname, delim_whitespace=True, header=None, names=['pid', 'type'])
polymer_particle_types = list(set(polymer_df.type.values))
binder_particle_types = ['%s_binder'%x for x in polymer_particle_types]
particle_types = polymer_particle_types + binder_particle_types

bond_types = ['polymer', 'loop_extrusion']
le_typeid = bond_types.index(le_type)

snap_args = {'N': 0, 'box': box,
             'particle_types': particle_types,
             'bond_types': bond_types}

# define polymers
polymers = []
pol_size = len(polymer_df)
for i in range(1): # In this version we assume to have only 1 polymer
    particles_typeid = np.array([particle_types.index(x) for x in polymer_df.type.values])
    pol0 = dict(size=pol_size, bonds_typeid=0,
                particles_typeid=particles_typeid,
                pbc=np.array([True, True, True]),
                rcm_pos=np.zeros(3))
    polymers.append(pol0)

# define binders
binder_sizes = [int(binder_bead_ratio*np.sum(particles_typeid==particle_types.index(x))) for x in polymer_particle_types]
binders = [
    dict(size=binder_sizes[binder_particle_types.index(bt)],
         particles_typeid=particle_types.index(bt))
    for bt in binder_particle_types]
